from django.shortcuts import render, redirect
from django.http import HttpResponse

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict


#  Local Import
from .models import GroceryItem

# Create your views here.
def index(request):
	groceryitem_list = GroceryItem.objects.all()
	context = {
		'groceryitem_list': groceryitem_list,
		'user': request.user
	}
	return render(request, "django_practice/index.html", context)
		
def groceryitems(request, groceryitem_id):
	# response = "You are viewing the details of %s"
	# return HttpResponse(response % groceryitem_id)

	groceryitem = model_to_dict(GroceryItem.objects.get(pk=groceryitem_id))
	return render(request, "django_practice/groceryitem.html", groceryitem)

def register(request):
	users = User.objects.all()
	is_user_registered = False

	context = {
		"is_user_registered": is_user_registered
	}

	set_firstname = "John"
	set_lastname = "Doe"
	set_email = f"{set_firstname}{set_lastname}@mail.com".lower()
	set_username = f"{set_firstname}{set_lastname}".lower()
	set_password = "john123"
	set_is_staff = False
	set_is_active = True


	for indiv_user in users:
		if indiv_user.username == set_username:
			is_user_registered = True
			break

	if is_user_registered == False:
		user = User()
		user.first_name = set_firstname
		user.last_name = set_lastname
		user.email = set_email
		user.username = set_username
		user.set_password(set_password)
		user.is_staff = set_is_staff
		user.is_active = set_is_active
		user.save()

		context = {
			"first_name": user.first_name,
			"last_name": user.last_name,
		}

	return render(request, "django_practice/register.html", context)

def change_password(request):
	is_user_authenticated = False
	get_username = "johndoe"
	get_password = "john123"
	new_password = "john1"

	user = authenticate(username = get_username, password = get_password)	
	print(user)

	if user is not None:
		authenticated_user = User.objects.get(username = get_username)
		authenticated_user.set_password(new_password)
		authenticated_user.save()
		is_user_authenticated = True

	context = {
		"is_user_authenticated": is_user_authenticated
	}

	return render(request, "django_practice/change_password.html", context)

def login_view(request):
	get_username = "johndoe"
	get_password = "john1"

	user = authenticate(username=get_username, password=get_password)
	print(user)			

	context = {
		"is_user_authenticated": False
	}

	if user is not None:
		login(request, user)
		return redirect("index")
	else:
		return render(request, "django_practice/login.html", context)

def logout_view(request):
	logout(request)
	return redirect(index)
