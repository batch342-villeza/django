# By running makemigrations, you’re telling Django that you’ve made some changes to your models (in this case, you’ve made new ones) and that you’d like the changes to be stored as a migration.
python manage.py makemigrations todolist

# To manage the migrations for our todolist app
python manage.py sqlmigrate todolist 0001

# To check for any changes in the migrations to apply them to our database
python manage.py migrate

# Opening the Django API
python manage.py shell

# Imports the models created in the todolist > models.py file
# Upon exiting the python shell, we will need to define this line again for the Python shell to determine the model/table we are looking into.
from todolist.models import ToDoItem;

# Retrieves all objects of the ToDoItem table
ToDoItem.objects.all()

# Imports timezone from the django.utils package to use for generating the date of the package
# Upon exiting the python shell, this will only be needed to defined again if the timezone will be used to create another record
from django.utils import timezone;

# Creates a variable "todoitem" in which to store the information on the record to be created
todoitem = ToDoItem(task_name="Eat", description="Dinner time, order pizza with extra cheese", date_created=timezone.now())

# Creates a record of the todoitem create above
todoitem.save()

# Accesses the different field of the newly created todoitem record
todoitem.id
todoitem.task_name
todoitem.description
todoitem.status
todoitem.date_created

# Updating object attributes
todoitem.status = "completed"
todoitem.save()

# Used to filter objects by their id
# Returns a querySet much like an array and requires the index number to access the attributes of the object
todoitem = ToDoItem.objects.filter(id=1)
todoitem[0].id
todoitem[0].task_name
todoitem[0].description
todoitem[0].status
todoitem[0].date_created

# Install mysqlclient and mysql_connector
# $ pip install mysqlclient
# $ pip install mysql_connector

#Create a new database named "python_db" in phpmyadmin

# Ensures that any changes to the models will be implemented when the tables are migrated
# $ python manage.py makemigrations todolist

# Creates the tables in the phpmyadmin
# $ python manage.py migrate`

# Django Admin
#   1. Creating a super user for Djang Admin from the command line
#       $ winpty python manage.py createsuperuser
#       Username (leave blank to use 'braia'): admin
#       Email address: admin@mail.com
#       Password: admin123
#   2. To access Django Admin
#       http://127.0.0.1:8000/admin/
#   3. from todolist app enter use the below codes
from .models import ToDoItem
admin.site.register(ToDoItem)

# 


    

